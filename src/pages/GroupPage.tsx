import React, { useEffect } from 'react';
import { ExpenseComponent } from '../components/ExpenseComponent';
import { localStorageService } from '../services/localStorage.service';
// import { generateRandomId } from '../utils/randomIDGenerator';
// import { Group } from '../models/Group.model.';

const GroupPage: React.FC = () => {
  useEffect(() => {
    document.title = `${group.title} - Splitty`;
  });

  // const newGroup: Group = {
  //   id: generateRandomId(),
  //   title: 'Piso',
  //   description: 'Gastos compartidos piso',
  //   participants: [],
  //   expenses: []
  // }

  // localStorageService.saveGroup(newGroup)

  const group = localStorageService.getGroup();

  return (
    <div className="GroupPage">
      <div className="group-heading">
        <h1>{group.title}</h1>
        <p>{group.description}</p>
      </div>
      <h3>
        <b></b>
      </h3>
      <div>
        <p>
          <b>Splitters: </b>
          {group.participants.map((participant) => participant.name).join(', ')}
        </p>
      </div>
      <div className="expenses">
        <h5>Gastos:</h5>
        <div>
          {group.expenses
            .sort((a, b) => b.date.valueOf() - a.date.valueOf())
            .map((expense) => (
              <ExpenseComponent expense={expense} key={expense.id} />
            ))}
        </div>
      </div>
    </div>
  );
};

export default GroupPage;
