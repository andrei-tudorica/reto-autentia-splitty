import React, { useCallback, useRef, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { User } from '../models/User.model.';
import { localStorageService } from '../services/localStorage.service';
import { generateRandomId } from '../utils/randomIDGenerator';
import { Routes } from '../constants/routes';

const NewFriend: React.FC = () => {
  const group = localStorageService.getGroup();
  const navigate = useNavigate();

  useEffect(() => {
    document.title = `Nuevo amigo - Splitty`;
  });
  const nameInputRef = useRef<HTMLInputElement>(null);

  const formHandler = useCallback(
    (event: React.FormEvent) => {
      event.preventDefault();

      if (nameInputRef.current !== null) {
        const newUser: User = {
          name: nameInputRef.current.value,
          id: generateRandomId(),
        };
        localStorageService.saveUser(newUser);
      }
      navigate(Routes.SPLITTY);
    },
    [navigate]
  );

  return (
    <div className="NewFriend">
      <div className='form-header'>
        <h1>Añade amigos</h1>
      </div>
      <form className="form" onSubmit={formHandler}>
        <label htmlFor="name">Nombre</label>
        <input
          type="text"
          id="name"
          ref={nameInputRef}
          placeholder="¿Cómo se llama tu amigo?"
        />
        <button type="submit">Añádelo</button>
      </form>
    </div>
  );
};

export default NewFriend;
