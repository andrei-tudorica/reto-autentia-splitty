import { DateTime } from 'luxon';
import React, { useCallback, useRef, useEffect } from 'react';
import { Expense } from '../models/Expense.model.';
import { generateRandomId } from '../utils/randomIDGenerator';
import { localStorageService } from '../services/localStorage.service';
import { Group } from '../models/Group.model.';
import { useNavigate } from 'react-router-dom';
import { Routes } from '../constants/routes';

const NewExpense: React.FC = () => {
  useEffect(() => {
    document.title = `Nuevo gasto - Splitty`;
  });

  const navigate = useNavigate();
  const descriptionInputRef = useRef<HTMLInputElement>(null);
  const amountInputRef = useRef<HTMLInputElement>(null);
  const paidBySelectRef = useRef<HTMLSelectElement>(null);
  const paidForSelectRef = useRef<HTMLSelectElement>(null);

  const group: Group = localStorageService.getGroup();
  console.log(group);

  const formHandler = useCallback(
    (event: React.FormEvent) => {
      event.preventDefault();

      if (
        amountInputRef.current !== null &&
        descriptionInputRef.current !== null &&
        paidBySelectRef.current !== null &&
        paidForSelectRef.current !== null
      ) {
        const paidBy = group.participants.find(
          (user) => user.id === parseInt(paidBySelectRef.current!.value)
        );

        const arrayFromHTMLCollection = Array.from(
          paidForSelectRef.current.selectedOptions
        );

        const selectedArray = arrayFromHTMLCollection.map(
          (option) => option.value
        );
        console.log('xxxxxxxxxx', selectedArray);

        const paidFor = group.participants.filter((participant) =>
          selectedArray.includes(participant.id.toString())
        );
        console.log('----paidfor----', paidFor);

        const newExpense: Expense = {
          amountInEuro: parseInt(amountInputRef.current.value),
          description: descriptionInputRef.current.value,
          paidBy: paidBy!,
          paidFor: paidFor,
          date: DateTime.now(),
          id: generateRandomId(),
        };
        localStorageService.saveNewExpense(newExpense);
        console.log('XXXXXXX', newExpense);
      }
      navigate(Routes.SPLITTY);
    },
    [group.participants, navigate]
  );

  return (
    <div className="NewExpense">
      <div className='form-header'>
        <h1>Añade un gasto</h1>
      </div>
      <form className="form" onSubmit={formHandler}>
        <label htmlFor="description">Gasto</label>
        <input
          type="text"
          id="description"
          ref={descriptionInputRef}
          placeholder="¿Qué has pagado?"
        />
        <label htmlFor="amount">Cantidad</label>
        <input
          type="number"
          id="amount"
          ref={amountInputRef}
          placeholder="¿Cuánto has pagado?"
        />
        <label htmlFor="paidBy">Pagado por</label>
        <div className="select-input">
          <select ref={paidBySelectRef} name="users" id="userThatPaid">
            <option value="0">Selecciona Usuario</option>
            {group.participants.map((participant) => {
              return (
                <option key={participant.id} value={participant.id}>
                  {participant.name}
                </option>
              );
            })}
          </select>
          <span className="focus"></span>
        </div>
        <label htmlFor="paidFor">Dividido entre</label>
        <div className="select-input select-multiple">
          <select
            ref={paidForSelectRef}
            name="users"
            id="usersPaidFor"
            multiple
          >
            {group.participants.map((participant) => {
              return (
                <option key={participant.id} value={participant.id}>
                  {participant.name}
                </option>
              );
            })}
          </select>
          <span className="focus"></span>
        </div>
        <button type="submit">Añádelo</button>
      </form>
    </div>
  );
};

export default NewExpense;
